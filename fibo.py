#! /usr/bin/env python3

def fibonacci_prime(N):
    if N < 1:
        return None

    F = [0, 1, 1, 2]
    i = 0
    n = 4

    while i < N - 1:
        F.append(F[n - 2] + F[n - 1])

        is_prime = True
        for d in range(2, F[-1]):
            if F[-1] % d == 0:
                is_prime = False
                break

        if is_prime:
            i += 1

        n += 1

    return F[-1]
