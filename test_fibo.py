#! /usr/bin/env python3

from fibo import fibonacci_prime

def test_fibo():
    assert fibonacci_prime(0) is None
    assert fibonacci_prime(1) == 2
    assert fibonacci_prime(5) == 89 
